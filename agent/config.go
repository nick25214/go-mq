package agent

import (
	"gitlab.com/nick25214/go-mq/glob"

	"github.com/Sirupsen/logrus"

	stringutil "gitlab.com/nick25214/nick-lib/util/string"
)

type AgentConfig struct {
	RcvLimitPerTick     int
	RcvTickSecond       int
	SendLimitPerTick    int
	SendTickSecond      int
	QueueUpperBound     int64
	WaitTickMicroSecond int
	MqPoolSize          int
}

func initConfig() *AgentConfig {
	c := glob.LoadConfig()
	agentCfg := &AgentConfig{}
	agentCfg.RcvLimitPerTick = stringutil.Atoi(c.GetValue("agent", "rcv_limit_per_tick", "100"), 100)
	agentCfg.RcvTickSecond = stringutil.Atoi(c.GetValue("agent", "rcv_tick_second", "1"), 1)
	agentCfg.SendLimitPerTick = stringutil.Atoi(c.GetValue("agent", "send_limit_per_tick", "100"), 100)
	agentCfg.SendTickSecond = stringutil.Atoi(c.GetValue("agent", "send_tick_second", "1"), 1)
	agentCfg.QueueUpperBound = int64(stringutil.Atoi(c.GetValue("agent", "queue_upper_bound", "10000"), 10000))
	agentCfg.WaitTickMicroSecond = stringutil.Atoi(c.GetValue("agent", "wait_tick_microsecond", "100"), 100)
	agentCfg.MqPoolSize = stringutil.Atoi(c.GetValue("agent", "mq_pool", "10"), 10)
	level := c.GetValue("log", "level", "error")

	if l, err := logrus.ParseLevel(level); err != nil {
		logrus.SetLevel(logrus.ErrorLevel)
	} else {
		logrus.SetLevel(l)
	}

	return agentCfg
}
